# Phoenix Plugin

## Installation
```javascript
import Phoenix from 'shared/plugins/phoenix'

Vue.use(new Phoenix({
  connection: 'wss://vue-phoenix.herokuapp.com/socket',
  debug: true,
  options: {
    heartbeatIntervalMs: 5000,
  },
}), {})
```

## Usage:

```javascript
new Vue({
    data() {
        return {
          messages: [],
        }
    },
    channels: {
        // Statical connection to channels and subscription to events
        'room:lobby': {
            join() {
              console.log("Joined lobby channel room:lobby");
            },
            shout(data) {
              this.messages.push(data.payload);
            },
        },
    },
    methods: {
        pushToChannel() {
            this.$vuePhoenix.pushToChannel('room:lobby', 'shout', {
                message: 'Phoenix hello!',
                sender: 'Sender1',
            });
        },
    },
    beforeCreate() {
        // this.$vuePhoenix.joinChannel('room:2'); --- join to channel dynamically
    },
    beforeDestroy() {
        // this.$vuePhoenix.leaveChannel('room:2') --- leave channel dynamically
    },
})
```

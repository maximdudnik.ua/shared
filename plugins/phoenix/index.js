import { Socket } from 'phoenix';
import Logger from './logger';
import Emitter from './emitter';
import Listener from './listener';
import Mixin from './mixin';

export default class Phoenix {
  /**
   * @param connection
   * @param debug
   * @param options
   */
  constructor({ connection, debug, options }) {
    Logger.debug = debug;
    this.socket = new Socket(connection, {
      ...options,
      logger: (kind, msg, data) => Logger.info(`${kind}, msg:"${msg}", data:`, data),
    });
    this.socket.connect();
    this.emitter = new Emitter();
    this.listener = new Listener(this.socket, this.emitter);
  }

  /**
   * Vue.js entry point
   * @param Vue
   * @param options
   */
  install(Vue, options) {
    Vue.prototype.$socket = this.socket;
    Vue.prototype.$vuePhoenix = this;
    Vue.mixin(Mixin);

    Logger.info('Phoenix plugin enabled');
  }

  /**
   * @param topic
   */
  joinChannel(topic) {
    return this.socket.channel(topic).join();
  }

  /**
   * @param topic
   */
  leaveChannel(topic) {
    const channel = this.socket.channels.find(ch => ch.topic === topic);

    if (channel) {
      channel.leave();
    }
  }

  /**
   * @param topic
   * @param event
   * @param payload
   * @param timeout
   */
  pushToChannel(topic, event, payload = {}) {
    let channel = this.socket.channels.find(ch => ch.topic === topic);

    if (!channel) {
      channel = this.joinChannel(topic);
    }

    channel.push(event, payload);
  }

}

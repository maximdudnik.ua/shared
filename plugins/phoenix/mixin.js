export default {

  // /**
  //  *  Assign runtime callbacks
  //  */
  // beforeCreate() {
  //
  //   if (!this.channels) this.channels = {};
  //
  //   // console.log("\n\n PHOENIX  beforeCreate\n\n", this.phoenix)
  //
  //   // this.sockets.subscribe = (event, callback) => {
  //   //   this.$vueSocketIo.emitter.addListener(event, callback, this);
  //   // };
  //   //
  //   // this.sockets.unsubscribe = (event) => {
  //   //   this.$vueSocketIo.emitter.removeListener(event, this);
  //   // };
  //
  // },

  /**
   * Join all channels and register events
   */
  mounted() {
    if (this.$options.channels) {
      const defaultEvents = [
        'join',
        'error',
        'close'
      ];

      Object.keys(this.$options.channels).forEach((topic) => {
        const channel = this.$vuePhoenix.joinChannel(topic);

        const joinCb = this.$options.channels[topic].join;
        if (typeof joinCb === 'function') {
          channel.receive('ok', () => {
            joinCb.apply(this);
          });
        }

        Object.keys(this.$options.channels[topic]).forEach((event) => {
          if (defaultEvents)
          this.$vuePhoenix.emitter.addListener(`${topic}-${event}`, this.$options.channels[topic][event], this);
        });
      });
    }
  },

  /**
   * Leave channels
   */
  beforeDestroy() {
    if (this.$options.channels) {
      Object.keys(this.$options.channels).forEach((topic) => {

        this.$vuePhoenix.leaveChannel(topic);

        Object.keys(this.$options.channels[topic]).forEach((event) => {
          this.$vuePhoenix.emitter.removeListener(`${topic}-${event}`, this);
        });

        this.$vuePhoenix.emitter.removeListener(`${topic}-join`, this);
        this.$vuePhoenix.emitter.removeListener(`${topic}-close`, this);
        this.$vuePhoenix.emitter.removeListener(`${topic}-error`, this);
      });
    }
  },
};

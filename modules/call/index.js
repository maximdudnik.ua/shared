import Component from './index.vue'
import store from './store'

export default {
    install(Vue, options ={}) {
        if (!options.store) {
            throw Error('Store not provided')
        }

        Vue.component('call', Component)
        options.store.registerModule('call', store)
    }
}

# Modules

## Usage:
1.1 Import and install as Vue plugin
```
import store from 'path/to/store'
import Call from 'shared/modules/call'
import Chat from 'shared/modules/chat'

Vue.use(Call, { store })
Vue.use(Chat, { store })
```
1.2 Use inside Vue components
```
<template>
  <div>
    <call></call> <!--  Shared module Call -->
    <chat></chat> <!--  Shared module Chat -->
  </div>
</template>
```

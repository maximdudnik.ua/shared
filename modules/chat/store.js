export default {
    namespaced: true,
    state: {
        count: 3
    },
    mutations: {
        increment(state) {
            state.count++;
        }
    },
    getters: {
        doubleCount(state) {
            return state.count * 2;
        }
    },
    actions: {

    }
};

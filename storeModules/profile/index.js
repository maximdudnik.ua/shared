import getters from './getters'
import mutations from './mutations'
import actions from './actions'

export default {
  namespaced: true,
  state: {
    uid: '123',
    name: 'Max'
  },
  getters,
  mutations,
  actions
}

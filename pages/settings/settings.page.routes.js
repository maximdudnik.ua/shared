const SettingsPage = () => import('./SettingsPage.vue');
const SettingsProfilePage = () => import('./subpages/SettingsProfilePage.vue');
const SettingsSystemPage = () => import('./subpages/SettingsSystemPage.vue');

export default [
    {
        path: '/settings', component: SettingsPage, redirect: '/settings/profile',
        children: [
            {
                path: 'profile',
                component: SettingsProfilePage
            },
            {
                path: 'system',
                component: SettingsSystemPage
            },
        ]
    }
]

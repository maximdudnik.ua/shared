# Pages

## Installation
```javascript
import SettingsPage from 'shared/pages/settings';
import router from 'path/to/router';

Vue.use(SettingsPage, { router });
```
